#!/bin/bash

cat > "cloud-config.yaml" <<EOF
{"ignition":{"config":{},"security":{"tls":{}},"timeouts":{},"version":"2.2.0"},"networkd":{},"passwd":{"users":[{"name":"core","sshAuthorizedKeys":["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnBu9rhJ684FSVPk4YJiP/og7wnK+VchcuIbhhGkw+VDu0bw481alPA19GDALRjD5KPTmrs4lFkcQBJYS8JdyoATM+GeAJCft9/Xi36X8Xbecv4IsmM6fln/EyMomNsi51IvPcgM1HhoP4Y6F084qgDl7mkkBRWPwItSUuB2ftM3ER5bMLkR+viomE+uRYcrG7usK1FwuCe0I4gz1pzzNzu8ddqx41eu1msAX2dzxOz0rnAsCmRA9cQmfp7gNI/O6bo89AOfXdqrpCsmi0j6uaGsxzKTiTqYzbe8n3DXHxnsHTrIy3pom1J97ZY9006UiZTTpc7xJmtDPej6DzOB9J mmaugust@localhost.localdomain","ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDRuhsP1Rcr+zZjCDACB2MTtE7BfgrJiawoUaPa8OK+rdZXqm9iCuaxPGTk4XaB9+u/O8EYwYQnYHHn9GTjYyZiM7UeU0AxvabCVzSoQyMAoDEFY+MiYNV0k9bB+A3L1/UBFlY4jB4srQl1L5ToWVxk3EtdIRqsuEpE9nCTHXUO/R+bXD+jeRsreB5o9UkAy9O/8mwMKYQRZhnkN37ii8aC1z7hzG85zJ1AUgHjW7vepe+EYgszitzvrBX+2ht8D3fxAyjVkIbEj6l1LedZQuaqmz/QteXloMEim8LTcdl0ra4WS31LMGHJAhNbfUlsTRvDNjPXALcJq4WBXrr0k/xZQhcCsjW8pfEzcaWXWddIkZz7X7GH6R2YUwwlDog3JsnZI6K8+3k2krJ8k0Y/fWAbKGLaujSyJtUovKXQZ2ZLOnWMmCf5gLiI3PsANzTbHMsaUer9o7RxwGczprUICoCsCNZdgSa2pqtiTI9VaaPrCVpIFSgYCrMP8dSpD/IGa5JK6KieX4V7ZdB8s3q20BEumEAeiHLxsId8fhNQzFcCrZvXl73DG5WfIfsbw1i05otHo+xItZU0Ujpx+eN6Kb5OxCENsbIQmZesh7UQOrfOFSqwLu4XdYLF1UVFRMVi45k1ifk4FngWp+7QwMlaENx+4I2iRX98spAo8vCt9JaSgQ== openpgp:0x3E7F150B"]}]},"storage":{},"systemd":{"units":[{"mask":true,"name":"update-engine.service"},{"mask":true,"name":"locksmithd.service"}]}}
EOF

sudo flatcar-install -d /dev/vda -i cloud-config.yaml -C beta -V 2513.2.0
sudo reboot
